{CompositeDisposable} = require 'atom'
path            = require 'path'
fs              = require 'fs'

class SimpleBackgroundView

  constructor: (serializedState) ->
    # Create the background element
    @background = document.createElement('div')
    @backgroundInner = document.createElement('div')
    @backgroundInner.classList.add('simple-background')
    @background.appendChild(@backgroundInner)
    @character = document.createElement('div')
    @character.classList.add('character-image')
    @background.appendChild(@character)
    @character.id = 'characterImageDiv'
    @cursorListeners={}

    atom.workspace.onDidChangeActiveTextEditor( =>
        activeEditor=atom.workspace.getActiveTextEditor()
        if activeEditor
          @setCursorListener(activeEditor)
          @refresh()
          #console.log('titlechanged '+atom.workspace.getActiveTextEditor().buffer.file.getBaseName())
    )

    @setCursorListener(atom.workspace.getActiveTextEditor())

    @refresh()

  setCursorListener: (activeEditor) =>
    filename=activeEditor.buffer.file.getBaseName()
    if !@cursorListeners[filename]
      @cursorListeners[filename] = activeEditor.onDidChangeCursorPosition( (event) =>
          if atom.workspace.getActiveTextEditor()
            #console.log('cursorposition event ',event)
            if !event.oldBufferPosition? or !event.newBufferPosition?
              @refresh()
              return
            oldCursorIndex=Math.floor(event.oldBufferPosition.row/50)
            newCursorIndex=Math.floor(event.newBufferPosition.row/50)
            if oldCursorIndex!=newCursorIndex
              @refresh()
      )

  refresh: =>
    bg=fs.readdirSync(__dirname+'/../bg_images')
    characterImages=fs.readdirSync(__dirname+'/../character_images')
    filename=atom.workspace.getActiveTextEditor().buffer.file.getBaseName()
    #console.log('set new lastfilename ',filename)
    fileindex=getRandom(filename,bg.length)
    whichfile=bg[fileindex]
    image_url=__dirname+'/../bg_images/'+whichfile
    #console.log "testing sb "+image_url+" "+bg.length+" "+characterImages.length+" "+filename+" "+fileindex+" "+whichfile
    @backgroundInner.style.backgroundImage = "url(\"" + image_url + "\")"

    cursor_position=Math.floor(atom.workspace.getActiveTextEditor().getCursorBufferPosition()['row']/50)
    filename=atom.workspace.getActiveTextEditor().buffer.file.getBaseName()+cursor_position
    fileindex=getRandom(filename,characterImages.length)
    whichfile=characterImages[fileindex]
    image_url=__dirname+'/../character_images/'+whichfile
    #console.log "testing character image "+image_url+" "+fileindex
    @character.style.backgroundImage = "url(\"" + image_url + "\")"
  # Returns an object that can be retrieved when package is activated
  serialize: ->

  # Tear down any state and detach
  destroy: ->
    @background.remove()


  getElement: ->
    @background

  #https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
  `function xmur3(str) {
    for(var i = 0, h = 1779033703 ^ str.length; i < str.length; i++)
        h = Math.imul(h ^ str.charCodeAt(i), 3432918353),
        h = h << 13 | h >>> 19;
    return function() {
        h = Math.imul(h ^ h >>> 16, 2246822507);
        h = Math.imul(h ^ h >>> 13, 3266489909);
        return (h ^= h >>> 16) >>> 0;
    }
  }
  function xoshiro128ss(a, b, c, d) {
    return function() {
        var t = b << 9, r = a * 5; r = (r << 7 | r >>> 25) * 9;
        c ^= a; d ^= b;
        b ^= c; a ^= d; c ^= t;
        d = d << 11 | d >>> 21;
        return (r >>> 0) / 4294967296;
    }
  }
  function getRandom(seed,n){
      var seed=xmur3(seed)
      var rand=xoshiro128ss(seed(),seed(),seed(),seed())
      return Math.floor(rand()*n)
  }`

module.exports = SimpleBackgroundView
