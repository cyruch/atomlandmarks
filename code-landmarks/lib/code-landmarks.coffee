SimpleBackgroundView = require './code-landmarks-view'
{CompositeDisposable} = require 'atom'

module.exports = SimpleBackground =
  simpleBackgroundView: null
  subscriptions: null

  activate: (state) ->
    @simpleBackgroundView = new SimpleBackgroundView(state.simpleBackgroundViewState)
    document.body.appendChild @simpleBackgroundView.getElement()

    @subscriptions = new CompositeDisposable
    @subscriptions.add atom.commands.add 'atom-workspace', 'code-landmarks:refresh': @simpleBackgroundView.refresh

  deactivate: ->
    @subscriptions.dispose()
    @simpleBackgroundView.destroy()

  serialize: ->
    simpleBackgroundViewState: @simpleBackgroundView.serialize()
